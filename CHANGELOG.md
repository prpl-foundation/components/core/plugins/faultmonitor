# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.19 - 2024-10-23(07:57:13 +0000)

### Other

- [REG]Sentry - No crashes upload since 0.10.58

## Release v1.0.18 - 2024-10-18(13:59:26 +0000)

### Other

- [REG]Sentry - No crashes upload since 0.10.58

## Release v1.0.17 - 2024-09-30(20:38:21 +0000)

## Release v1.0.16 - 2024-09-13(09:15:24 +0000)

### Other

- - [ProcessFaults] Device.DeviceInfo.ProcessFaults. is not reboot persistence and Device.DeviceInfo.ProcessFaults.ProcessFault.{i}.FirmwareVersion is empty

## Release v1.0.15 - 2024-09-11(16:16:44 +0000)

### Other

- LIBBART: klocwork issue

## Release v1.0.14 - 2024-09-10(07:07:36 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.0.13 - 2024-08-13(10:42:44 +0000)

### Other

- - [ProcessFaults] ProcessFault.{i].Remove() command is not implemented

## Release v1.0.12 - 2024-08-13(10:32:45 +0000)

### Other

- - [ProcessFaults] MaxProcessFaultEntries capping not behaving as expected (round robin iso capping)

## Release v1.0.11 - 2024-08-13(10:21:55 +0000)

### Other

- - [ProcessFaults] ProcessFault timestamp is unix epoch instead of dateTime

## Release v1.0.10 - 2024-08-13(10:16:41 +0000)

### Other

- - [FaultProcess] The HGW failed to add instances under the fault object when insufficient space was detected

## Release v1.0.9 - 2024-07-30(14:30:13 +0000)

### Other

- Add tr181-device proxy odl files to various components

## Release v1.0.8 - 2024-07-29(06:13:30 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v1.0.7 - 2024-07-08(07:04:46 +0000)

## Release v1.0.6 - 2024-07-05(13:24:45 +0000)

### Other

- amx plugin should not run as root user

## Release v1.0.5 - 2024-06-19(12:33:24 +0000)

### Other

- make available on gitlab.com

## Release v1.0.4 - 2024-05-16(14:29:40 +0000)

### Other

- Faults should be flushed on reset and upgrade

## Release v1.0.3 - 2024-04-10(10:11:25 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.0.2 - 2024-04-08(14:37:35 +0000)

### Fixes

- [FaultProcess] FaultProcess stored in /ext/faults directory despite insufficient space

## Release v1.0.1 - 2024-03-21(17:29:58 +0000)

### Fixes

- Replace ba-cli calls with ubus in the coredump handling script

## Release v1.0.0 - 2024-02-23(08:54:52 +0000)

### Breaking

- Implementation of the new 2.17 BBF Data model definition DeviceInfo.ProcessFaults

### Fixes

- [FaultMonitior] FaultNumberOfEntries can exceed MaximumCoreDumps

## Release v0.4.4 - 2024-02-16(09:40:00 +0000)

## Release v0.4.3 - 2023-11-07(09:30:20 +0000)

### Changes

- [faultmonitor] Implement defaults.d directory

## Release v0.4.2 - 2023-11-03(08:18:18 +0000)

### Fixes

- [faultmonitor] Mismatch between dumps kept and displayed in DM

## Release v0.4.1 - 2023-10-13(14:11:10 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.4.0 - 2023-10-12(11:28:13 +0000)

### New

- Update Licenses in oss components

## Release v0.3.0 - 2023-10-11(12:36:25 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.2.1 - 2023-09-07(10:54:39 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.2.0 - 2023-08-31(09:57:20 +0000)

### New

- Move datamodel prefixes to the device proxy

## Release v0.1.6 - 2023-08-31(09:14:59 +0000)

### Fixes

- coredumps not stored if plugin is not running

## Release v0.1.5 - 2023-06-16(09:46:18 +0000)

### Fixes

- [faultmonitor] FaultNumberOfEntries > MaximumCoreDumps

## Release v0.1.4 - 2023-06-05(13:53:58 +0000)

### Fixes

- [faultmonitor] faultmonitor doesn't store coredumps generated during boot

## Release v0.1.3 - 2023-06-01(10:56:34 +0000)

### Other

- Rename component to amx-faultmonitor

## Release v0.1.2 - 2023-05-25(16:38:12 +0000)

### Other

- Fault instances are not persistent [Fix]

## Release v0.1.1 - 2023-05-15(12:14:35 +0000)

### Fixes

- faultmonitor doesn't start at boot

## Release v0.1.0 - 2023-05-11(11:20:49 +0000)

### New

- [Faultmonitor] implement the faultmonitor plugin

