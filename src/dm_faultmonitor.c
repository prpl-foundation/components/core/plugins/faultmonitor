/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "faultmonitor.h"
#define ME DEFAULT_TRACE_ZONE_NAME

int remove_r(const char* path) {
    int retval = 1;
    struct dirent* file_info = NULL;
    char* file_path = NULL;
    DIR* dir = NULL;
    size_t path_len = 0;

    when_null(path, exit);

    dir = opendir(path);
    when_null(dir, exit);

    path_len = strlen(path);
    file_info = readdir(dir);
    retval = 0;
    while(file_info != NULL && !retval) {
        if((strcmp(file_info->d_name, ".") == 0) || (strcmp(file_info->d_name, "..") == 0)) {
            file_info = readdir(dir);
            continue;
        }
        file_path = calloc(path_len + strlen(file_info->d_name) + 2, sizeof(char));
        when_null_trace(file_path, error, ERROR, "memory allocation failure");

        sprintf(file_path, "%s/%s", path, file_info->d_name);
        if(file_info->d_type == DT_DIR) {
            retval = remove_r(file_path);
        } else {
            retval = remove(file_path);
        }
        free(file_path);
        file_info = readdir(dir);
    }
    closedir(dir);
    if(retval == 0) {
        retval = rmdir(path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to remove folder %s", path);
    }

exit:
    return retval;

error:
    return 1;

}

static amxd_status_t rm_fault_instances(amxd_object_t* root_object) {
    amxd_object_t* faults = amxd_object_get_child(root_object, "ProcessFault");
    amxd_object_t* fault = NULL;
    amxd_trans_t transaction;
    amxd_status_t retval = amxd_status_unknown_error;

    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, faults);
    amxd_object_iterate(instance, it_fault, faults) {
        fault = amxc_container_of(it_fault, amxd_object_t, it);
        amxd_trans_del_inst(&transaction, amxd_object_get_index(fault), NULL);
    }
    retval = amxd_trans_apply(&transaction, get_dm());
    amxd_trans_clean(&transaction);
    return retval;
}

amxd_status_t _RemoveAllProcessFaults(amxd_object_t* root_object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    const char* path_dumps = GET_CHAR(amxd_object_get_param_value(root_object, "StoragePath"), NULL);

    when_null_trace(path_dumps, exit, ERROR, "Failed to retrieve path of the core dumps directory");
    retval = rm_fault_instances(root_object);
    when_failed_trace(retval, exit, ERROR, "Failed to remove Fault instances from datamodel");
    if(!remove_r(path_dumps)) {
        retval = amxd_status_ok;
    }
exit:
    return retval;
}

amxd_status_t _Remove(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxd_trans_t transaction;
    amxd_object_t* parent_object = NULL;
    amxd_status_t retval = amxd_status_unknown_error;
    const char* path_dumps = NULL;

    path_dumps = GET_CHAR(amxd_object_get_param_value(object, "FaultLocation"), NULL);
    when_true_trace(remove_r(path_dumps), exit, ERROR, "Failed to delete dump");

    retval = amxd_trans_init(&transaction);
    when_failed(retval, exit);

    parent_object = amxd_object_get_parent(object);
    when_null_trace(parent_object, exit, ERROR, "Failed to get parent object");

    retval = amxd_trans_select_object(&transaction, parent_object);
    when_failed(retval, exit);

    amxd_trans_del_inst(&transaction, amxd_object_get_index(object), NULL);
    retval = amxd_trans_apply(&transaction, get_dm());

exit:
    amxd_trans_clean(&transaction);
    return retval;
}

amxd_status_t _Upload(UNUSED amxd_object_t* root_object,
                      UNUSED amxd_function_t* func,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    return amxd_status_function_not_implemented;
}

static void remove_oldest_instance(amxd_object_t* fault_obj, uint32_t nb_instance_to_delete) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, fault_obj);

    amxd_object_iterate(instance, it_fault, fault_obj) {
        amxd_object_t* fault = NULL;
        const char* path_dumps = NULL;

        fault = amxc_container_of(it_fault, amxd_object_t, it);
        amxd_trans_del_inst(&trans, amxd_object_get_index(fault), NULL);

        path_dumps = GET_CHAR(amxd_object_get_param_value(fault, "FaultLocation"), NULL);
        remove_r(path_dumps);

        nb_instance_to_delete--;

        if(nb_instance_to_delete == 0) {
            break;
        }
    }

    amxd_trans_apply(&trans, get_dm());
    amxd_trans_clean(&trans);
}

static void param_increment(const char* path) {
    amxd_param_t* param = NULL;
    amxc_var_t value;
    amxd_object_t* root_object = get_object("ProcessFaults.");
    uint32_t ivalue = 0;

    amxc_var_init(&value);

    param = amxd_object_get_param_def(root_object, path);
    when_null_trace(param, exit, ERROR, "Failed to get %s", path);

    amxd_param_get_value(param, &value);
    when_failed_trace(amxd_param_get_value(param, &value), exit, ERROR, "Failed to get %s value", path);
    ivalue = amxc_var_dyncast(uint32_t, &value) + 1;

    amxc_var_set(uint32_t, &value, ivalue);
    amxd_param_set_value(param, &value);

exit:
    amxc_var_clean(&value);
    return;
}

void _process_faults_changed(UNUSED const char* const sig_name,
                             UNUSED const amxc_var_t* const data,
                             UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    amxc_var_t* params = NULL;
    params = amxc_var_get_key(data, "parameters", AMXC_VAR_FLAG_DEFAULT);

    if(params != NULL) {
        amxc_var_t* max_process_fault_entries_var = NULL;
        max_process_fault_entries_var = amxc_var_get_key(params, "MaxProcessFaultEntries", AMXC_VAR_FLAG_DEFAULT);

        if(max_process_fault_entries_var != NULL) {
            amxd_object_t* object = get_object("ProcessFaults.ProcessFault.");
            uint32_t fault_entries_nb = amxd_object_get_instance_count(object);
            uint32_t max_process_fault_entries = GET_UINT32(max_process_fault_entries_var, "to");

            if(fault_entries_nb > max_process_fault_entries) {
                remove_oldest_instance(object, fault_entries_nb - max_process_fault_entries);
            }
        }
    }

    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _faults_add(amxd_object_t* const object,
                          amxd_param_t* const p,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {

    amxd_status_t status = amxd_status_unknown_error;
    amxd_dm_access_t access = amxd_dm_access_public;

    access = (amxd_dm_access_t) amxc_var_dyncast(uint32_t, GET_ARG(args, "access"));

    if(access != amxd_dm_access_private) {
        amxc_var_t* params = NULL;
        amxc_var_t* fault_location_var = NULL;
        amxc_var_t* auto_delete_var = NULL;
        const char* fault_location = NULL;
        bool auto_delete = false;

        params = amxc_var_get_key(args, "parameters", AMXC_VAR_FLAG_DEFAULT);

        when_null_trace(params, exit, ERROR, "No parameters object found");

        auto_delete_var = amxo_parser_get_config(get_parser(), "auto_delete");
        when_null_trace(auto_delete_var, exit, ERROR, "Can't get auto_delete");
        auto_delete = amxc_var_dyncast(bool, auto_delete_var);

        fault_location_var = amxc_var_get_key(params, "FaultLocation", AMXC_VAR_FLAG_DEFAULT);

        when_null_trace(fault_location_var, exit, ERROR, "No FaultLocation object found");

        fault_location = GET_CHAR(fault_location_var, NULL);

        param_increment("LastUpgradeCount");
        param_increment("CurrentBootCount");

        when_str_empty_status(fault_location, exit, status = amxd_status_invalid_value);

        status = amxd_status_ok;

        if(auto_delete == false) {
            amxd_object_t* parent_object = NULL;
            uint32_t max_process_fault_entries = 0;
            uint32_t fault_entries_nb = 0;

            parent_object = amxd_object_get_parent(object);
            when_null_trace(parent_object, exit, ERROR, "Failed to get parent object");
            max_process_fault_entries = amxd_object_get_value(uint32_t, parent_object, "MaxProcessFaultEntries", NULL);
            fault_entries_nb = amxd_object_get_instance_count(object);

            if(fault_entries_nb >= max_process_fault_entries) {
                status = amxd_status_invalid_value;
            }
        }

        when_failed_trace(status, exit, ERROR, "Validation failed: don't add instance");
    }

    status = amxd_action_object_add_inst(object, p, reason, args, retval, priv);

exit:
    return status;
}

void _faults_added(UNUSED const char* const sig_name,
                   UNUSED const amxc_var_t* const data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    bool auto_delete = amxc_var_dyncast(bool, amxo_parser_get_config(get_parser(), "auto_delete"));

    when_false(auto_delete, exit);

    amxc_var_t value;
    amxd_param_t* param = NULL;
    amxd_object_t* object = NULL;
    amxd_object_t* parent_object = NULL;
    uint32_t fault_entries_nb = 0;
    uint32_t max_process_fault_entries = 0;

    object = get_object("ProcessFaults.ProcessFault.");
    fault_entries_nb = amxd_object_get_instance_count(object);

    parent_object = amxd_object_get_parent(object);
    when_null_trace(parent_object, exit, ERROR, "Failed to get parent object");
    param = amxd_object_get_param_def(parent_object, "MaxProcessFaultEntries");
    when_null_trace(parent_object, exit, ERROR, "Failed to get MaxProcessFaultEntries");

    amxc_var_init(&value);
    if(amxd_param_get_value(param, &value) == amxd_status_ok) {
        max_process_fault_entries = amxc_var_dyncast(uint32_t, &value);

        if(fault_entries_nb > max_process_fault_entries) {
            remove_oldest_instance(object, fault_entries_nb - max_process_fault_entries);
        }
    }
    amxc_var_clean(&value);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _faults_start(UNUSED const char* const sig_name,
                   UNUSED const amxc_var_t* const data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    amxd_param_t* param = NULL;
    amxc_var_t value;
    amxd_object_t* root_object = get_object("ProcessFaults.");
    uint32_t ivalue = 0;

    amxc_var_init(&value);

    // Assign PreviousBootCount with the CurrentBootCount value
    // Reset CurrentBootCount
    param = amxd_object_get_param_def(root_object, "CurrentBootCount");
    when_null_trace(param, exit, ERROR, "Failed to get CurrentBootCount");
    when_failed_trace(amxd_param_get_value(param, &value), exit, ERROR, "Failed to get CurrentBootCount value");
    ivalue = amxc_var_dyncast(uint32_t, &value);

    amxc_var_set(uint32_t, &value, 0);
    amxd_param_set_value(param, &value);

    param = amxd_object_get_param_def(root_object, "PreviousBootCount");
    when_null_trace(param, exit, ERROR, "Failed to get PreviousBootCount");
    amxc_var_set(uint32_t, &value, ivalue);
    amxd_param_set_value(param, &value);

exit:
    amxc_var_clean(&value);

    SAH_TRACEZ_OUT(ME);
    return;
}
