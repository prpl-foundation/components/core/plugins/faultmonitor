%define{
    %persistent object 'ProcessFaults' {
        on event "dm:object-changed" call process_faults_changed;
        /**
        * Path where the process fault logs must be stored.
        *
        * @version 1.0
        */
        %persistent %read-only string StoragePath="/ext/faults";
        /**
        * Total number of process faults which occured since last firmware upgrade.
        *
        * @version 1.0
        */
        %persistent %read-only uint32 LastUpgradeCount;
        /**
        * The number of process faults which occurred during the previous boot cycle.
        *
        * @version 1.0
        */
        %persistent %read-only uint32 PreviousBootCount;
        /**
        * The number of process faults which occurred during the previous boot cycle.
        *
        * @version 1.0
        */
        %private %persistent %read-only uint32 CurrentBootCount;
        /**
        * Minimum free space in KiB on the device that must be free
        * before attempting to create a ProcessFault instance.
        * This setting does not affect the counting of process faults.
        *
        * @version 1.0
        */
        %persistent uint32 MinFreeSpace = 3000;
        /**
        * The maximum number of entries available in the ProcessFault table.
        * Defines the maximum number of ProcessFault instances that
        * can be stored on the device.
        * If this limit is hit, No new ProcessFault instances will be created
        * until sufficient process fault instances are removed.
        * Set this parameter to 0 to completely disable the creation
        * of ProcessFault instances.
        * Reducing the MaxProcessFaultEntries will cause for the implementation
        * to delete the oldest ProcessFault instances.
        * This setting does not affect the counting of process faults.
        *
        * @version 1.0
        */
        %persistent uint32 MaxProcessFaultEntries = 5;
        /**
        * Each instance of this object describes a recorded fault.
        *
        * @version 1.0
        */
        %persistent object ProcessFault[]{
            on action add-inst call faults_add;
            /**
            * [Alias] A non-volatile handle used to reference this instance.
            * Alias provides a mechanism for an ACS to label this instance for future reference.
            * If the CPE supports the Alias-based Addressing feature as defined
            * in [Section 3.6.1/TR-069] and described in [Appendix II/TR-069],
            * the following mandatory constraints MUST be enforced:
            * Its value MUST NOT be empty.
            * Its value MUST start with a letter.
            * If its value is not assigned by the ACS, it MUST start with a “cpe-” prefix.
            * The CPE MUST NOT change the parameter value.
            * At most one entry in this table can exist with a given value for Alias.
            *
            * @version 1.0
            */
            %unique %key string Alias;
            /**
            * The number of entries in the ProcessFault table.
            *
            * @version 1.0
            */
            counted with ProcessFaultNumberOfEntries;
            /**
            * Process ID of the process that crashed.
            *
            * @version 1.0
            */
            %persistent %read-only uint32 ProcessID;
            /**
            * Process Name of the process that crashed.
            *
            * @version 1.0
            */
            %persistent %read-only string ProcessName;
            /**
            * Specifies the location of where to create and keep the process fault logs.
            * Can either be a relative path or file within the StoragePath location.
            *
            * @version 1.0
            */
            %persistent %read-only string FaultLocation;
            /**
            * The time when the process fault occured.
            *
            * @version 1.0
            */
            %persistent %read-only datetime TimeStamp;
            /**
            * The firmware version that triggered the the process fault.
            *
            * @version 1.0
            */
            %persistent %read-only string FirmwareVersion;
            /**
            * The command arguments that were used to start to the process.
            *
            * @version 1.0
            */
            %persistent %read-only string Arguments;
            /**
            * The reason why the process fault occurred.
            *
            * @version 1.0
            */
            %persistent %read-only uint32 Reason;
            /**
            * [ASYNC] This command is issued to upload the Fault information
            * specified by this Fault instance.
            * All results of the actual upload will be contained within
            * the LocalAgent.TransferComplete! event.
            *
            * @version 1.0
            */
            void Upload(%in %mandatory string URL, %in %mandatory string Username, %in %mandatory string Password);
            /**
            * [ASYNC] Remove this row from the table,
            * together with the associated faulty data stored on the device.
            *
            * @version 1.0
            */
            void Remove();
        }
        /**
        * [ASYNC] This command removes all the ProcessFault.{i}.
        * from the device, together with all the associated
        * faulty data stored on the device.
        *
        * @version 1.0
        */
        void RemoveAllProcessFaults();
    }
}

%populate {
    on event "dm:instance-added" call faults_added
        filter 'path == "ProcessFaults.ProcessFault."';
    on event "app:start" call faults_start;
}
