%config {
    %global privileges = {
        user = "USER_ID", group = "GROUP_ID", capabilities = ["CAP_SETUID", "CAP_SETGID", "CAP_SETPCAP", "CAP_DAC_OVERRIDE"]
    };
}