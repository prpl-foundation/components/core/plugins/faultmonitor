#!/bin/sh

COREBASEDIR=/ext/faults

get_config_dm() {
    params=$(ubus call ProcessFaults _get)
    if [ -n "$params" ]; then
        local value=""
        value=$(jsonfilter -s "$params" -e '@[@].StoragePath')
        [ -n "$value" ] && COREBASEDIR=$value
    fi
}

get_config_dm

if [ -n "$COREBASEDIR" -a -d "$COREBASEDIR" ]; then
    echo "Flushing $COREBASEDIR content"
    rm -rf $COREBASEDIR/*
fi
