#!/bin/sh
FAULTBASEDIR=/ext/faults

. /usr/lib/amx/scripts/amx_init_functions.sh

show_cmd()
{
    echo "[$(date)]: $@"
    eval "$@"
}

debuginfo()
{
    show_cmd cat /proc/sys/kernel/core_pattern
    if [ -e $FAULTBASEDIR/count ]; then
        show_cmd cat $FAULTBASEDIR/count
    fi
    if [ -e $FAULTBASEDIR/log ]; then
        show_cmd cat $FAULTBASEDIR/log
    fi
}

case $1 in
	boot)
		echo "|/usr/lib/amx-faultmonitor/amx-faultmonitor_record.sh %p %s %t %P" > /proc/sys/kernel/core_pattern
        echo 10 > /proc/sys/kernel/core_pipe_limit
	;;
	start)
	;;
	stop)
		echo "core" > /proc/sys/kernel/core_pattern
	;;
	debuginfo)
		debuginfo
	;;
	reset)
	;;
	*)
		echo "Usage : $0 [start|stop|debuginfo]"
	;;
esac
