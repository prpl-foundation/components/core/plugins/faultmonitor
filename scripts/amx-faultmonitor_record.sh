#! /bin/sh

# Monitor and store core dumps of crashing processes

# default configuration
COREBASEDIR=/ext/faults
MINFREESPACE=3000
MAXCOREDUMPS=20
LOCKDIR=/tmp/faultmonitor.lock
UPLOADSERVER=""
UPLOADTIMEOUT=60
MAXLOGENTRIES=100


get_config_dm() {
    params=$(ubus call ProcessFaults _get)

    if [ -n "$params" ]; then
      local value=""
      value=$(jsonfilter -s "$params" -e '@[@].MaxProcessFaultEntries')
      [ -n "$value" ] && MAXCOREDUMPS=$value
      value=$(jsonfilter -s "$params" -e '@[@].StoragePath')
      [ -n "$value" ] && COREBASEDIR=$value
      value=$(jsonfilter -s "$params" -e '@[@].MinFreeSpace')
      [ -n "$value" ] && MINFREESPACE=$value
    fi
}

get_config_dm

if [  -z "$COREBASEDIR" ]; then
    exit 0
fi


# Basic characteristics of the fault
PID=$1
SIGNAL=$2
TIME=$(date -u -d @$3 '+%Y-%m-%dT%H:%M:%SZ')

if [ ! -z "$4" ]; then
	PID=$4
fi

# For systems where the arguments are seperated by null bytes, translate
# those in semi-colons (;).
CMDLINE=$(tr \\0 ";" < /proc/$PID/cmdline)

# Do not handle crash dumps of ourself
if grep -q faultmonitor /proc/$PID/cmdline; then
   exit 0
fi

# Compute the crash id and store the count of crashes
mkdir -p $COREBASEDIR
while ! mkdir $LOCKDIR 2> /dev/null; do
  # poor mans busy waiting mutex
  sleep 0
done
COUNTFILE=$COREBASEDIR/count
CRASHID=0
if [ -e $COUNTFILE ]; then
  CRASHID=$(cat $COUNTFILE)
fi
COUNT=$(expr $CRASHID + 1)
# Store counter/crash id
echo $COUNT > $COUNTFILE
rmdir $LOCKDIR

# Store in log
FAULTSLOG=$COREBASEDIR/log
LOGENTRIES=0
if [ -f $FAULTSLOG ]; then
    LOGENTRIES=$(wc -l $FAULTSLOG |awk '{print $1}')
    if [ $LOGENTRIES -ge $MAXLOGENTRIES ]; then
    # remove the oldest entries
    sed -i "1d" $FAULTSLOG
    fi
fi

# Compute local core dump path and check limits
# note: those checks are racy
COREDUMPPATH=$COREBASEDIR/dump/$CRASHID/
FREESPACE=$(\df -k $COREBASEDIR | tail -f -n 1 | awk "{ print \$4 }")
if [ $FREESPACE -lt $MINFREESPACE ]; then
    COREDUMPPATH=""
fi
if [ $MAXCOREDUMPS -eq 0 ]; then
    COREDUMPPATH=""
fi

echo "$CRASHID $PID $SIGNAL $TIME $CMDLINE" >> $FAULTSLOG

# Retreive software version
params=$(ubus call DeviceInfo _get)
if [ -n "$params" ]; then
    SOFTWAREVERSION=$(jsonfilter -s "$params" -e '@[@].SoftwareVersion')
fi

# Store details
RET=$(ubus call ProcessFaults.ProcessFault _add "\
    {\"parameters\":\
        {\"Reason\": $SIGNAL,\
        \"ProcessName\": \"$CMDLINE\",\
        \"FaultLocation\": \"$COREDUMPPATH\",\
        \"ProcessID\": \"$PID\",\
        \"TimeStamp\": \"$TIME\",\
        \"FirmwareVersion\": \"$SOFTWAREVERSION\"}}")

# Extract the amxd-error-code
amxd_error_code=$(echo "$RET" | sed -n 's/.*"amxd-error-code": \([0-9]*\).*/\1/p')

if ! test -z "$COREDUMPPATH" && [ "$amxd_error_code" -eq 0 ]; then
    mkdir -p $COREDUMPPATH
    for i in wchan maps smaps fdinfo "stat*"; do
       echo cp -r /proc/$PID/$i $COREDUMPPATH
       cp -r /proc/$PID/$i $COREDUMPPATH
    done
    ls -l /proc/$PID/fd/ > $COREDUMPPATH/fd-listing.txt
    gzip -c > $COREDUMPPATH/core.gz
fi


