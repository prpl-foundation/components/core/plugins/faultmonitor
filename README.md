FaultMonitor
============

Introduction and High-Level Description
---------------------------------------

The FaultMonitor component identifies process crashes within the data model. It provides essential information, including the date and process name. Additionally, core dumps are stored for future retrieval and analysis. The data model also allows for configuration of the number of core dumps to retain.
            

References
----------


Configuring code dumps and collecting crash information:`[https://sigquit.wordpress.com/2009/03/13/the-core-pattern/](https://sigquit.wordpress.com/2009/03/13/the-core-pattern/)

Architecture
==============

![Faultmonitor Architecture diagram](doc/image/FaultMonitorArchitecture.png)

Implementation
==============

Process fault monitor core dumps collection relies on kernel core\_pattern with pipe mechanism to a script to gather core dumps and all relevant information.

Upon starting the process will set `/proc/sys/kernel/core_pattern` to `|/usr/lib/amx-faultmonitor/amx-faultmonitor_record.sh %p %s %t %P`, its collection script.

Thus, in order, the script parameters will be: process pid, signal number, UNIX time of dump, pid of the process in its own namespace (useful for containerized applications).

Note default path of collection script is configurable through the main odl with `installation_path` config.

Collection script is in the gitlab project here : [https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/blob/main/scripts/amx-faultmonitor\_record.sh?ref\_type=heads](https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/blob/main/scripts/amx-faultmonitor_record.sh?ref_type=heads)

Data Model
----------

This plugins uses standard TR-181 datamode as defined here : https://usp-data-models.broadband-forum.org/tr-181-2-18-1-usp.html#D.Device:2.Device.DeviceInfo.ProcessFaults 

Low-Level API
-------------

N/A

API
---

N/A

Files
-----

### Configurations

Component datamodel definition and default configuration is installed in `/etc/amx/amx-faultmonitor`

### core dump directory structure

By default in configuration the core dump directory points to `/ext/faults/`

There are 2 files by defaults:

*   `count`: contains a simple integer of number of core dumps
    
*   `log`: contains history of core dumps with the parameter passed on to amx-faultmonitor\_record.sh  
    Example:
```    
    sequence, process pid, signal number, UNIX time of dump, command line
    0 5353 11 2024-08-26T20:09:39Z gmap-mod-upnp;-D;/etc/amx/gmap-client/modules/gmap-mod-upnp.odl;
    1 4743 11 2024-09-09T11:13:17Z tr181-device;-D;
```    

Core dumps and related material from procfs will be stored in a `/ext/faults/dump/%d/` directory where %d it the id of the dump.

Example:
```
root@prplOS:# ls -1 /ext/faults/dump/0/
core.gz                   --> zipped core dump file
fd-listing.txt            --> listing of /proc/<PID>/fd/ at time of the dump
fdinfo                    --> copy of /proc/<PID>/fdinfo at time of the dump
maps                      --> copy of /proc/<PID>/maps at time of the dump
smaps                     --> copy of /proc/<PID>/smaps at time of the dump
stat                      --> copy of /proc/<PID>/stat at time of the dump
statm                     --> copy of /proc/<PID>/statm at time of the dump
status                    --> copy of /proc/<PID>/status at time of the dump
wchan                     --> copy of /proc/<PID>/whan at time of the dump
```
Firewall
--------

N/A

Dependants and Dependencies
---------------------------

### Dependants

none

### Dependencies

FaultMonitor depends on the following libraries:

*   **libsahtrace** is used for debugging traces
    
*   **mod-dmext** is used for the regexp function in the odl file
    

FaultMonitor is an Ambiorix application and as such it has the following dependencies on Ambiorix libraries:

*   **libamxa**
    
*   **libamxb**
    
*   **libamxc**
    
*   **libamxd**
    
*   **libamxo**
    
*   **libamxp**
    
*   **cap-ng**
    

Configuration
-------------

*   You can set default for `MaxProcessFaultEntries` , `MinFreeSpace` and `StoragePath` in following file:
    

[https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/blob/main/odl/defaults.d/00\_amx-faultmonitor\_defaults.odl?ref\_type=heads](https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/blob/main/odl/defaults.d/00_amx-faultmonitor_defaults.odl?ref_type=heads)

*   Round Robin behavior for collecting core dumps:
    

It is possible to select a round-robin behavior for core dumps entries instead of a capping behavior (mandated by TR-181) through an odl configuration.

For this `auto_delete = true;` need to be set in the main component odl.

Backup/Restore, Upgrade, Reset
------------------------------

Core dumps, and corresponding datamodel entries, are wiped out upon upgrade or reset.


Considerations
==============

Security
--------

Core dumps may contain privacy-sensitive and security-sensitive, information. The privacy-sensitive aspects may be subject to legal and regulatory rules, in addition to internal rules within the Operator.


Quality
=======

Unit Tests
----------

[https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/tree/main/test?ref\_type=heads](https://gitlab.com/prpl-foundation/components/core/plugins/faultmonitor/-/tree/main/test?ref_type=heads)

Logging and Debugging
---------------------

Logging is done through the sahtrace library machanism. See [https://gitlab.com/prpl-foundation/components/core/modules/mod-sahtrace/-/blob/main/README.md?ref\_type=heads](https://gitlab.com/prpl-foundation/components/core/modules/mod-sahtrace/-/blob/main/README.md?ref_type=heads) .

Available traces for process monitor:
```
ProcessFaults.list\_trace\_zones() returned
\[
    {
        faultmonitor = "200"
    }
\]
```

As kernel crash logs will already appear by default in system logs. Faultmonitor won’t add an additional specific trace to log that a process crash occurred.

Enhancement of this section would be to write down implemented errors/warning messages and their meaning



CI Tests
--------

Process fault monitor in CI in the main CRAM testbed:

[https://gitlab.com/prpl-foundation/prplos/prplos/-/blob/mf/mainline-3.1/.gitlab/tests/cram/generic/acceleration-plan-components/026-processfaults-monitor.t?ref\_type=heads](https://gitlab.com/prpl-foundation/prplos/prplos/-/blob/mf/mainline-3.1/.gitlab/tests/cram/generic/acceleration-plan-components/026-processfaults-monitor.t?ref_type=heads)

Principle of the testing is to cause a crash by sending a SIGSEGV to a dummy process and check that the core dump is properly collected.

As the CI also as a mechanism to collect core dumps at end of the testing some additional code is added to make sure those are kept if any crash were to happen before the component test.

