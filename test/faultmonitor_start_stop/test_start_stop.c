/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_parameter.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include <amxo/amxo.h>

#include "test_start_stop.h"
#include "faultmonitor.h"

#define MOCK_DUMP_FOLDER "./mock_dump"

static const char* odl_defs = "test.odl";
static const char* odl_defs_auto_delete = "test_auto_delete.odl";

static amxd_dm_t* dm;
static amxo_parser_t* parser;

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static int create_mock() {
    struct stat st = {0};
    int retval = 1;
    if(stat(MOCK_DUMP_FOLDER, &st) == -1) {
        retval = mkdir(MOCK_DUMP_FOLDER, 0755);
    }
    return retval;
}

static int remove_mock() {
    struct stat st = {0};
    int retval = 1;
    if(stat(MOCK_DUMP_FOLDER, &st) == 0) {
        retval = remove_r(MOCK_DUMP_FOLDER);
    }
    return retval;
}

static int dump_process(const char* proc_name) {
    int retval = 0;
    char path_dump[512] = {0};
    snprintf(path_dump, 512, "%s/%s", MOCK_DUMP_FOLDER, proc_name);
    retval |= mkdir(path_dump, 0755);
    snprintf(path_dump, 512, "%s/%s/procinfo", MOCK_DUMP_FOLDER, proc_name);
    retval |= mkdir(path_dump, 0755);
    return retval;
}

static void resolve_functions(void) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "faults_start",
                                            AMXO_FUNC(_faults_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "faults_add",
                                            AMXO_FUNC(_faults_add)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "faults_added",
                                            AMXO_FUNC(_faults_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "RemoveAllProcessFaults",
                                            AMXO_FUNC(_RemoveAllProcessFaults)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_faults_changed",
                                            AMXO_FUNC(_process_faults_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Upload",
                                            AMXO_FUNC(_Upload)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Remove",
                                            AMXO_FUNC(_Remove)), 0);
}

static int internal_test_setup(void** state, const char* odl) {
    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    assert_non_null(dm);
    assert_non_null(parser);

    assert_int_equal(create_mock(), 0);
    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    resolve_functions();

    assert_int_equal(amxo_parser_parse_file(parser, odl, root_obj), 0);

    handle_events();
    return 0;
}

int test_setup(void** state) {
    return internal_test_setup(state, odl_defs);
}

int test_setup_auto_delete(void** state) {
    return internal_test_setup(state, odl_defs_auto_delete);
}

int test_teardown(UNUSED void** state) {
    amxo_parser_clean(parser);
    amxd_dm_clean(dm);
    remove_mock();

    amxut_bus_handle_events();
    amxut_bus_teardown(state);
    return 0;
}

void test_can_start_plugin(UNUSED void** state) {
    assert_int_equal(_faultmonitor_main(0, dm, parser), 0);

    assert_non_null(get_dm());
    assert_ptr_equal(get_dm(), dm);

    assert_non_null(get_parser());
    assert_ptr_equal(get_parser(), parser);

    assert_non_null(get_object("ProcessFaults."));
}

void test_can_stop_plugin(UNUSED void** state) {
    assert_int_equal(_faultmonitor_main(1, dm, parser), 0);
    assert_null(get_dm());
    assert_null(get_parser());
}

static void add_fault_inst(amxd_trans_t* trans, const char* FaultLocation) {
    amxc_var_t data;
    amxc_var_t* params = NULL;

    amxd_trans_select_pathf(trans, "ProcessFaults.ProcessFault.");

    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "FaultLocation", FaultLocation);

    amxd_trans_add_action(trans, action_object_add_inst, &data);

    amxc_var_clean(&data);
}

void test_removedumps(UNUSED void** state) {
    struct stat st = {0};
    amxd_trans_t trans;
    amxd_object_t* root_object = get_object("ProcessFaults.");
    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    add_fault_inst(&trans, "dummy");
    add_fault_inst(&trans, "dummy");
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    handle_events();
    assert_int_equal(dump_process("proc1"), 0);
    assert_int_equal(dump_process("proc2"), 0);
    assert_int_not_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL), 0);
    assert_int_equal(amxd_object_set_cstring_t(root_object, "StoragePath", MOCK_DUMP_FOLDER), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);
    handle_events();
    assert_int_equal(stat(MOCK_DUMP_FOLDER, &st), -1);
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL), 0);
    amxd_trans_clean(&trans);
    assert_int_equal(create_mock(), 0);
}

void test_removedump(UNUSED void** state) {
    struct stat st = {0};
    amxd_trans_t trans;
    amxc_var_t ret;
    char path_instance_to_delete[100];
    const amxc_llist_it_t* it = NULL;
    amxd_object_t* fault_object = NULL;
    amxd_object_t* root_object = get_object("ProcessFaults.");
    amxd_object_t* faults_object = get_object("ProcessFaults.ProcessFault.");
    uint32_t fault_object_to_delete = 0;

    assert_non_null(root_object);
    assert_non_null(faults_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    add_fault_inst(&trans, MOCK_DUMP_FOLDER "/proc1");
    add_fault_inst(&trans, MOCK_DUMP_FOLDER "/proc2");
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    handle_events();
    assert_int_equal(dump_process("proc1"), 0);
    assert_int_equal(dump_process("proc2"), 0);
    assert_int_not_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL), 0);
    assert_int_equal(amxd_object_set_cstring_t(root_object, "StoragePath", MOCK_DUMP_FOLDER), 0);


    amxc_var_init(&ret);
    it = amxd_object_first_instance(faults_object);
    assert_non_null(it);
    fault_object = amxc_llist_it_get_data(it, amxd_object_t, it);
    assert_non_null(fault_object);
    fault_object_to_delete = amxd_object_get_index(fault_object);
    sprintf(path_instance_to_delete, "ProcessFaults.ProcessFault.%d", fault_object_to_delete);
    assert_int_equal(amxb_call(amxut_bus_ctx(), path_instance_to_delete, "Remove", NULL, &ret, 5), 0);

    handle_events();

    assert_int_equal(stat(MOCK_DUMP_FOLDER "/proc1", &st), -1);
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL), 1);
    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
}

void test_adddumps(UNUSED void** state) {
    amxd_trans_t trans;
    uint32_t nbfault = 0;
    amxd_object_t* root_object = get_object("ProcessFaults.");

    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);
    nbfault = amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL);

    while(nbfault < (amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL))) {
        add_fault_inst(&trans, "dummy");
        nbfault++;
    }

    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    handle_events();
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL),
                     amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL));
    amxd_trans_clean(&trans);

    assert_int_equal(amxd_trans_init(&trans), 0);
    add_fault_inst(&trans, "dummy");
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), amxd_status_invalid_value);
    handle_events();
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL),
                     amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL));
    amxd_trans_clean(&trans);
}

void test_adddumps_auto_delete(UNUSED void** state) {
    amxd_trans_t trans;
    uint32_t nbfault = 0;
    amxd_object_t* root_object = get_object("ProcessFaults.");

    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);
    nbfault = amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL);

    while(nbfault < (amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL) + 2)) {
        add_fault_inst(&trans, "dummy");
        nbfault++;
    }

    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    handle_events();
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL),
                     amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL));
    amxd_trans_clean(&trans);
}

void test_change_MaxProcessFaultEntries(UNUSED void** state) {
    amxd_trans_t trans;
    uint32_t nbfault = 0;
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* root_object = get_object("ProcessFaults.");

    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);
    nbfault = amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL);

    while(nbfault < (amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL))) {
        add_fault_inst(&trans, "dummy");
        nbfault++;
    }

    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    handle_events();
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL),
                     amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL));
    amxd_trans_clean(&trans);

    amxc_var_init(&ret);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "MaxProcessFaultEntries", 3);
    assert_int_equal(amxb_set(amxut_bus_ctx(), "ProcessFaults.", &data, &ret, 5), 0);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);

    handle_events();

    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL),
                     amxd_object_get_value(uint32_t, root_object, "MaxProcessFaultEntries", NULL));
}

void test_adddumps_with_empty_fault_location(UNUSED void** state) {
    amxd_trans_t trans;
    uint32_t nbfault = 0;
    amxd_object_t* root_object = get_object("ProcessFaults.");

    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);

    nbfault = amxd_object_get_value(uint32_t, root_object, "LastUpgradeCount", NULL);

    add_fault_inst(&trans, "");

    assert_int_equal(amxd_trans_apply(&trans, get_dm()), amxd_status_invalid_value);

    handle_events();

    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "ProcessFaultNumberOfEntries", NULL), 0);
    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "LastUpgradeCount", NULL), nbfault + 1);

    amxd_trans_clean(&trans);
}

void test_PreviousBootCount(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* root_object = get_object("ProcessFaults.");

    assert_non_null(root_object);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(_RemoveAllProcessFaults(root_object, NULL, NULL, NULL), 0);

    add_fault_inst(&trans, "");
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), amxd_status_invalid_value);

    handle_events();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    handle_events();

    assert_int_equal(amxd_object_get_value(uint32_t, root_object, "PreviousBootCount", NULL), 1);

    amxd_trans_clean(&trans);
}